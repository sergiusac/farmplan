﻿using FarmPlanApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Services
{
    class PlanDateService
    {
        public static PlanDate FindPlanDate(Plan plan, string date)
        {
            var splitted = date.Split('.');
            var year = int.Parse(splitted[0]);
            var month = int.Parse(splitted[1]);
            return plan.Dates.Where(p => p.Year == year && p.Month == month).First();
        }
    }
}
