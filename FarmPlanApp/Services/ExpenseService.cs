﻿using FarmPlanApp.Db;
using FarmPlanApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Services
{
    public class ExpenseService
    {
        public static Expense CreateExpense(float value, ExpenseType expenseType, PlanDate planDate)
        {
            var newExpense = new Expense();
            newExpense.Value = value;
            newExpense.ExpenseType = expenseType;
            newExpense.PlanDate = planDate;

            var ctx = AppDbContext.GetContext();
            ctx.Expenses.Add(newExpense);
            ctx.SaveChanges();

            return newExpense;
        }
    }
}
