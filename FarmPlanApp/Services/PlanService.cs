﻿using FarmPlanApp.Db;
using FarmPlanApp.Entities;
using FarmPlanApp.MVs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Services
{
    public class PlanService
    {
        public static void CreatePlan(PlanMV plan)
        {
            var context = AppDbContext.GetContext();

            var newPlan = new Plan();
            newPlan.Name = plan.Name;

            for (int i = plan.StartYear; i <= plan.EndYear; i++)
            {
                var isFirstYear = i == plan.StartYear;
                var isLastMonth = i == plan.EndYear;
                var startMonth = isFirstYear ? plan.StartMonth : 1;
                var endMonth = isLastMonth ? plan.EndMonth : 12;

                for (int j = startMonth; j <= endMonth ; j++)
                {
                    newPlan.Dates.Add(new PlanDate() { Year = i, Month = j });
                }
            }

            context.Plans.Add(newPlan);
            context.SaveChanges();
        }

        public static int[] GetYears(Plan plan)
        {
            return plan.Dates.GroupBy(date => date.Year).Select(date => date.Key).ToArray();
        }

        public static int[] GetMonths(Plan plan, int year)
        {
            return plan.Dates.Where(date => date.Year == year).Select(date => date.Month).ToArray();
        }

        public static PlanDate GetPlanDate(Plan plan, int year, int month)
        {
            return plan.Dates.Where(date => date.Year == year && date.Month == month).First();
        }

        public static float GetSumOfExpenses(Plan plan, ExpenseType type, int year, int month)
        {
            var planDate = GetPlanDate(plan, year, month);
            var filtered = planDate.Expenses.Where(e => e.ExpenseType.Equals(type));
            if (filtered.Count() > 0)
            {
                return filtered.Aggregate((x, y) => new Expense { Value = x.Value + y.Value }).Value;
            }
            else
            {
                return 0;
            }
        }

        public static float GetSumOfExpenses(Plan plan, int year, int month)
        {
            var planDate = GetPlanDate(plan, year, month);
            if (planDate.Expenses.Count > 0)
            {
                return planDate.Expenses.Aggregate((x, y) => new Expense() { Value = x.Value + y.Value }).Value;
            }
            else
            {
                return 0;
            }
        }

        public static Sale GetSumOfSales(Plan plan, int year, int month)
        {
            var planDate = GetPlanDate(plan, year, month);
            if (planDate.Sales.Count > 0)
            {
                return planDate.Sales.Aggregate((x, y) => new Sale()
                {
                    Volume = x.Volume + y.Volume,
                    Price = x.Price + y.Price
                });
            }
            else
            {
                return null;
            }
        }

        public static void RemovePlan(Plan plan)
        {
            var ctx = AppDbContext.GetContext();
            ctx.Plans.Remove(plan);
            ctx.SaveChanges();
        }
    }
}
