﻿using FarmPlanApp.Db;
using FarmPlanApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Services
{
    public class ExpenseTypeService
    {
        public static void CreateExpenseType(string name, Plan plan)
        {
            var context = AppDbContext.GetContext();

            var newType = new ExpenseType();
            newType.TypeName = name;
            newType.Plan = plan;

            context.ExpenseTypes.Add(newType);
            context.SaveChanges();
        }

        public static ExpenseType FindExpenseType(Plan plan, string typeName)
        {
            return plan.ExpenseTypes.Where(t => t.TypeName.Equals(typeName)).First();
        }

        public static void RemoveExpenseType(ExpenseType type)
        {
            var ctx = AppDbContext.GetContext();
            ctx.ExpenseTypes.Remove(type);
            ctx.SaveChanges();
        }
    }
}
