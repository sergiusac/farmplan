﻿using FarmPlanApp.Db;
using FarmPlanApp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Services
{
    public class SaleService
    {
        public static Sale AddSale(PlanDate date, float volume, float price)
        {
            var sale = new Sale();
            sale.Volume = volume;
            sale.Price = price;
            sale.PlanDate = date;

            var ctx = AppDbContext.GetContext();
            ctx.Sales.Add(sale);
            ctx.SaveChanges();

            return sale;
        }
    }
}
