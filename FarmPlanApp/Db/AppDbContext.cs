﻿using FarmPlanApp.Entities;
using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Db
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<PlanDate> PlanDates { get; set; }
        public virtual DbSet<ExpenseType> ExpenseTypes { get; set; }
        public virtual DbSet<Expense> Expenses { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }

        private static AppDbContext dbContext;

        private AppDbContext() : base("default")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<AppDbContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);

            modelBuilder.Entity<Plan>()
                .HasMany(t => t.Dates)
                .WithOptional(t => t.Plan)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Plan>()
                .HasMany(t => t.ExpenseTypes)
                .WithOptional(t => t.Plan)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<ExpenseType>()
                .HasMany(t => t.Expenses)
                .WithOptional(t => t.ExpenseType)
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<PlanDate>()
                .HasMany(t => t.Sales)
                .WithOptional(t => t.PlanDate)
                .WillCascadeOnDelete(true);
        }

        public static AppDbContext GetContext()
        {
            if (dbContext == null)
            {
                dbContext = new AppDbContext();
            }

            return dbContext;
        }
    }
}
