﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Entities
{
    public class PlanDate
    {
        [Key]
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public Plan Plan { get; set; }
        public virtual ICollection<Expense> Expenses { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }

        public PlanDate()
        {
            Expenses = new List<Expense>();
            Sales = new List<Sale>();
        }
    }
}
