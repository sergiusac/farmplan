﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Entities
{
    public class Expense
    {
        [Key]
        public int Id { get; set; }
        public float Value { get; set; }
        public ExpenseType ExpenseType { get; set; }
        public PlanDate PlanDate { get; set; }
    }
}
