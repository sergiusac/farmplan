﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Entities
{
    public class Plan
    {
        [Key]
        public string Name { get; set; }
        public virtual ICollection<PlanDate> Dates { get; set; }
        public virtual ICollection<ExpenseType> ExpenseTypes { get; set; }

        public Plan()
        {
            Dates = new List<PlanDate>();
            ExpenseTypes = new List<ExpenseType>();
        }
    }
}
