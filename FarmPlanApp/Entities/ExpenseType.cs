﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Entities
{
    public class ExpenseType
    {
        [Key]
        public int Id { get; set; }
        public string TypeName { get; set; }
        public Plan Plan { get; set; }
        public virtual ICollection<Expense> Expenses { get; set; }

        public ExpenseType()
        {
            Expenses = new List<Expense>();
        }
    }
}
