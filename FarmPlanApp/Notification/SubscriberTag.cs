﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Notification
{
    public class SubscriberTag
    {
        public const string DEFAULT_TAG = "default";
        public const string ROOT_TAG = "root";
        public const string PLAN_VIEW_TAG = "plan_view";
    }
}
