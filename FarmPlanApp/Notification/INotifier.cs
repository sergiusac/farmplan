﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Notification
{
    public interface INotifier
    {
        void Subscribe(ISubscriber subscriber, string tag);
        void Notify(string tag);
    }
}
