﻿using FarmPlanApp.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Notification
{
    public class AppNotifier : INotifier
    {
        private const string DEFAULT_KEY = SubscriberTag.DEFAULT_TAG;

        private IDictionary<string, ICollection<ISubscriber>> subscribers;

        private static INotifier notifier;

        private AppNotifier() 
        {
            subscribers = new Dictionary<string, ICollection<ISubscriber>>();
        }

        public void Notify(string tag)
        {
            var key = tag.Length > 0 ? tag : DEFAULT_KEY;
            
            foreach (ISubscriber subscriber in subscribers[key])
            {
                subscriber.UpdateData();
            }
        }

        public void Subscribe(ISubscriber subscriber, string tag)
        {
            if (subscriber != null)
            {
                var key = tag.Length > 0 ? tag : DEFAULT_KEY;

                if (subscribers.ContainsKey(key))
                {
                    subscribers[key].Add(subscriber);
                }
                else
                {
                    subscribers[key] = new List<ISubscriber>();
                    subscribers[key].Add(subscriber);
                }
            }
        }

        public static INotifier GetNotifier()
        {
            if (notifier == null)
                notifier = new AppNotifier();
            return notifier;
        }
    }
}
