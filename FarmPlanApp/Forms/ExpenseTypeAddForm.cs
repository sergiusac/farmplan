﻿using FarmPlanApp.Entities;
using FarmPlanApp.Notification;
using FarmPlanApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmPlanApp.Forms
{
    public partial class ExpenseTypeAddForm : Form
    {
        private Plan plan;

        public ExpenseTypeAddForm(Plan plan)
        {
            InitializeComponent();

            this.plan = plan;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            var name = expenseTypeNameTextBox.Text;

            if (name.Trim().Length > 0)
            {
                ExpenseTypeService.CreateExpenseType(name, plan);
                AppNotifier.GetNotifier().Notify(SubscriberTag.PLAN_VIEW_TAG);
                Close();
            }
            else
            {
                MessageBox.Show("Некорректные данные");
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
