﻿using FarmPlanApp.Db;
using FarmPlanApp.Entities;
using FarmPlanApp.Notification;
using FarmPlanApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmPlanApp.Forms
{
    public partial class RootForm : Form, ISubscriber
    {

        public RootForm()
        {
            InitializeComponent();
        }

        private void addNewPlanBtn_Click(object sender, EventArgs e)
        {
            var planAddForm = new PlanAddForm();
            planAddForm.ShowDialog();
        }

        protected override void OnLoad(EventArgs e)
        {
            initDb();
            UpdateData();
            AppNotifier.GetNotifier().Subscribe(this, SubscriberTag.ROOT_TAG);
        }

        public void UpdateData()
        {
            var plans = AppDbContext.GetContext().Plans;
            plansListBox.DataSource = plans.ToList();
            plansListBox.ValueMember = "Name";
        }

        private void initDb()
        {
            var db = AppDbContext.GetContext();
            if (!File.Exists("data/app.db"))
            {
                db.Database.Initialize(true);
            }
        }

        private void plansListBox_DoubleClick(object sender, EventArgs e)
        {
            var planViewForm = new PlanViewForm((Plan)plansListBox.SelectedItem);
            planViewForm.ShowDialog();
        }

        private void removePlanBtn_Click(object sender, EventArgs e)
        {
            Plan planToDelete = (Plan) plansListBox.SelectedItem;
            DialogResult result = MessageBox.Show(
                "Вы действительно хотите удалить план " + planToDelete.Name + "?", 
                "Удалить план " + planToDelete.Name + "?", 
                MessageBoxButtons.YesNoCancel, 
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button3);
            if (result.Equals(DialogResult.Yes))
            {
                PlanService.RemovePlan(planToDelete);
                AppNotifier.GetNotifier().Notify(SubscriberTag.ROOT_TAG);
            }
        }
    }
}
