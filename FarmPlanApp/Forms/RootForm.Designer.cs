﻿namespace FarmPlanApp.Forms
{
    partial class RootForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addNewPlanBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.plansListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.removePlanBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addNewPlanBtn
            // 
            this.addNewPlanBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addNewPlanBtn.Location = new System.Drawing.Point(351, 253);
            this.addNewPlanBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addNewPlanBtn.Name = "addNewPlanBtn";
            this.addNewPlanBtn.Size = new System.Drawing.Size(96, 23);
            this.addNewPlanBtn.TabIndex = 1;
            this.addNewPlanBtn.Text = "Добавить план";
            this.addNewPlanBtn.UseVisualStyleBackColor = true;
            this.addNewPlanBtn.Click += new System.EventHandler(this.addNewPlanBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.plansListBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(439, 242);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Планирование";
            // 
            // plansListBox
            // 
            this.plansListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plansListBox.FormattingEnabled = true;
            this.plansListBox.Location = new System.Drawing.Point(4, 37);
            this.plansListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.plansListBox.Name = "plansListBox";
            this.plansListBox.Size = new System.Drawing.Size(432, 199);
            this.plansListBox.TabIndex = 1;
            this.plansListBox.DoubleClick += new System.EventHandler(this.plansListBox_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Список планов";
            // 
            // removePlanBtn
            // 
            this.removePlanBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removePlanBtn.Location = new System.Drawing.Point(251, 253);
            this.removePlanBtn.Margin = new System.Windows.Forms.Padding(2);
            this.removePlanBtn.Name = "removePlanBtn";
            this.removePlanBtn.Size = new System.Drawing.Size(96, 23);
            this.removePlanBtn.TabIndex = 3;
            this.removePlanBtn.Text = "Удалить план";
            this.removePlanBtn.UseVisualStyleBackColor = true;
            this.removePlanBtn.Click += new System.EventHandler(this.removePlanBtn_Click);
            // 
            // RootForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 284);
            this.Controls.Add(this.removePlanBtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.addNewPlanBtn);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "RootForm";
            this.Text = "RootForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button addNewPlanBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox plansListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button removePlanBtn;
    }
}