﻿using FarmPlanApp.Binds;
using FarmPlanApp.Db;
using FarmPlanApp.MVs;
using FarmPlanApp.Notification;
using FarmPlanApp.Services;
using FarmPlanApp.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmPlanApp.Forms
{
    public partial class PlanAddForm : Form
    {
        public PlanAddForm()
        {
            InitializeComponent();

            planStartMonthComboBox.DataSource = new BindingSource() { DataSource = Months.Values };
            planEndMonthComboBox.DataSource = new BindingSource() { DataSource = Months.Values };
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            PlanMV plan = new PlanMV()
            {
                Name = planNameTextBox.Text,
                StartYear = int.Parse(planStartYearTextBox.Text),
                StartMonth = int.Parse(planStartMonthComboBox.Text),
                EndYear = int.Parse(planEndYearTextBox.Text),
                EndMonth = int.Parse(planEndMonthComboBox.Text)
            };
            var validationResult = isInputDataValid(plan);

            if (validationResult)
            {
                PlanService.CreatePlan(plan);
                AppNotifier.GetNotifier().Notify(SubscriberTag.ROOT_TAG);
                Close();
            }
            else
            {
                MessageBox.Show("Некорректные данные");
            }

        }

        private bool isInputDataValid(PlanMV plan)
        {
            var validator = new PlanValidator();

            return validator.Validate(plan);
        }
    }
}
