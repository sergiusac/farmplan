﻿using FarmPlanApp.Db;
using FarmPlanApp.Entities;
using FarmPlanApp.Notification;
using FarmPlanApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmPlanApp.Forms
{
    public partial class PlanViewForm : Form, ISubscriber
    {
        private Plan plan;

        public PlanViewForm(Plan plan)
        {
            InitializeComponent();

            this.plan = plan;
        }

        protected override void OnLoad(EventArgs e)
        {
            AppNotifier.GetNotifier().Subscribe(this, SubscriberTag.PLAN_VIEW_TAG);
            UpdateData();
        }

        private void addExpenseBtn_Click(object sender, EventArgs e)
        {
            var expenseAddForm = new ExpenseAddForm(plan);
            expenseAddForm.ShowDialog();
        }

        private void addExpenseTypeBtn_Click(object sender, EventArgs e)
        {
            var addForm = new ExpenseTypeAddForm(plan);
            addForm.ShowDialog();
        }

        private void addIncomeBtn_Click(object sender, EventArgs e)
        {
            var incomeAddForm = new IncomeAddForm(plan);
            incomeAddForm.ShowDialog();
        }

        private void removeExpenseTypeBtn_Click(object sender, EventArgs e)
        {
            var expenseTypeRemoveForm = new ExpenseTypeRemoveForm(plan);
            expenseTypeRemoveForm.ShowDialog();
        }

        public void UpdateData()
        {
            UpdateExpensesDGV();
            UpdateIncomesDGV();
            UpdateSummariesDGV();
        }

        private void UpdateExpensesDGV()
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("Показатель");
            var planDates = plan.Dates;
            var expenseTypes = plan.ExpenseTypes;

            foreach (var planDate in planDates)
            {
                dataTable.Columns.Add(planDate.Year + "." + (planDate.Month < 10 ? "0" : "") + planDate.Month);
            }

            foreach (var expenseType in expenseTypes)
            {
                dataTable.Rows.Add(expenseType.TypeName);
            }
            dataTable.Rows.Add("Итого");

            int columnsCount = dataTable.Columns.Count;
            int rowsCount = dataTable.Rows.Count;
            for (int i = 1; i < columnsCount; i++)
            {
                var planDate = planDates.ToList()[i - 1];
                if (planDate.Expenses.Count > 0)
                {
                    float expensesForMonth = 0;
                    for (int j = 0; j < rowsCount - 1; j++)
                    {
                        var row = dataTable.Rows[j];
                        var typeName = (string)row[0];
                        var filtered = planDate.Expenses
                            .Where(e => e.ExpenseType.TypeName.Equals(typeName));
                        if (filtered.Count() > 0)
                        {
                            var expense = filtered.Aggregate((x, y) => new Expense { Value = x.Value + y.Value });
                            row[i] = string.Format("{0:n}", expense.Value);

                            expensesForMonth += expense.Value;
                        }
                    }
                    dataTable.Rows[rowsCount - 1][i] = string.Format("{0:n}", expensesForMonth);
                }
            }


            expensesDGV.DataSource = dataTable;
        }

        private void UpdateIncomesDGV()
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("Показатель");

            var planDates = plan.Dates;
            foreach (var planDate in planDates)
            {
                dataTable.Columns.Add(planDate.Year + "." + (planDate.Month < 10 ? "0" : "") + planDate.Month);
            }

            dataTable.Rows.Add("Объём товарного молока, кг");
            dataTable.Rows.Add("Цена, тнг/кг");

            DataRow rowVolume = dataTable.Rows[0];
            DataRow rowPrice = dataTable.Rows[1];
            int columnsCount = dataTable.Columns.Count;

            for (int i = 1; i < columnsCount; i++)
            {
                var planDate = planDates.ToList()[i - 1];
                if (planDate.Sales.Count > 0)
                {
                    var sale = planDate.Sales.Aggregate((x, y) => new Sale() 
                    {
                        Volume = x.Volume + y.Volume,
                        Price = x.Price + y.Price
                    });
                    rowVolume[i] = string.Format("{0:n}", sale.Volume);
                    rowPrice[i] = string.Format("{0:n}", sale.Price);
                }
            }

            incomesDGV.DataSource = dataTable;
        }

        public void UpdateSummariesDGV()
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("Показатель");

            var planDates = plan.Dates;
            foreach (var planDate in planDates)
            {
                dataTable.Columns.Add(planDate.Year + "." + (planDate.Month < 10 ? "0" : "") + planDate.Month);
            }

            dataTable.Rows.Add("Выручка, тнг");
            dataTable.Rows.Add("Всего переменных затрат в месяц, тнг");
            dataTable.Rows.Add("Сумма МД по календарным месяцам, тнг");
            dataTable.Rows.Add("Годовой объем маржинального дохода, тнг");

            int columnsCount = dataTable.Columns.Count;
            float annualNetProfit = 0;
            for (int i = 1; i < columnsCount; i++)
            {
                dataTable.Rows[0][i] = string.Format("{0:n}", CalcSummaryProfit(planDates.ElementAt(i - 1)));
                dataTable.Rows[1][i] = string.Format("{0:n}", CalcSummaryExpenses(planDates.ElementAt(i - 1)));
                float monthNetProfit = CalcNetProfit(planDates.ElementAt(i - 1));
                dataTable.Rows[2][i] = string.Format("{0:n}", monthNetProfit);
             
                annualNetProfit += monthNetProfit;
            }
            dataTable.Rows[3][1] = string.Format("{0:n}", annualNetProfit);

            summariesDGV.DataSource = dataTable;
        }

        private float CalcSummaryProfit(PlanDate planDate)
        {
            var monthSummarySale = PlanService.GetSumOfSales(plan, planDate.Year, planDate.Month);
            if (monthSummarySale != null)
            {
                return monthSummarySale.Volume * monthSummarySale.Price;
            }
            else
            {
                return 0;
            }
        }

        private float CalcSummaryExpenses(PlanDate planDate)
        {
            var monthSumOfExpenses = PlanService.GetSumOfExpenses(plan, planDate.Year, planDate.Month);
            var monthSummarySale = PlanService.GetSumOfSales(plan, planDate.Year, planDate.Month);
            if (monthSummarySale != null)
            {
                return monthSumOfExpenses * monthSummarySale.Volume;
            }
            else
            {
                return 0;
            }
        }

        public float CalcNetProfit(PlanDate planDate)
        {
            return CalcSummaryProfit(planDate) - CalcSummaryExpenses(planDate);
        }

        private float oldExpenseCellValue;
        private float newExpenseCellValue;

        private void expensesDGV_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            object value = expensesDGV[e.ColumnIndex, e.RowIndex].Value;
            try
            {
                oldExpenseCellValue = float.Parse(value.ToString());
            }
            catch (Exception ex)
            {
                oldExpenseCellValue = 0;
            }
        }

        private void expensesDGV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            object value = expensesDGV[e.ColumnIndex, e.RowIndex].Value;
            try
            {
                newExpenseCellValue = float.Parse(value.ToString());
                var columnName = expensesDGV.Columns[e.ColumnIndex].Name;
                var rowName = expensesDGV.Rows[e.RowIndex].Cells[0].Value.ToString();

                var expenseType = ExpenseTypeService.FindExpenseType(plan, rowName);
                var planDate = PlanDateService.FindPlanDate(plan, columnName);

                ExpenseService.CreateExpense(newExpenseCellValue - oldExpenseCellValue, expenseType, planDate);
                AppNotifier.GetNotifier().Notify(SubscriberTag.PLAN_VIEW_TAG);
            }
            catch (Exception ex)
            {
                if (!value.Equals(DBNull.Value))
                    MessageBox.Show("Неправильный формат данных");
            }
        }

        private float oldSaleVolume;
        private float newSaleVolume;
        private float oldSalePrice;
        private float newSalePrice;

        private void incomesDGV_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var volumeValue = incomesDGV[e.ColumnIndex, 0].Value;
            var priceValue = incomesDGV[e.ColumnIndex, 1].Value;

            try
            {
                oldSaleVolume = float.Parse(volumeValue.ToString());
                oldSalePrice = float.Parse(priceValue.ToString());

            }
            catch (Exception ex)
            {
                if (volumeValue.Equals(DBNull.Value))
                    oldSaleVolume = 0;
                if (priceValue.Equals(DBNull.Value))
                    oldSalePrice = 0;
            }
        }

        private void incomesDGV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var volumeValue = incomesDGV[e.ColumnIndex, 0].Value;
            var priceValue = incomesDGV[e.ColumnIndex, 1].Value;

            try
            {
                newSaleVolume = float.Parse(volumeValue.ToString());
                newSalePrice = float.Parse(priceValue.ToString());
            }
            catch (Exception ex)
            {
                if (volumeValue.Equals(DBNull.Value))
                    newSaleVolume = oldSaleVolume;
                if (priceValue.Equals(DBNull.Value))
                    newSalePrice = oldSalePrice;
            }

            var columnName = incomesDGV.Columns[e.ColumnIndex].Name;
            var planDate = PlanDateService.FindPlanDate(plan, columnName);

            SaleService.AddSale(planDate, newSaleVolume - oldSaleVolume, newSalePrice - oldSalePrice);
            AppNotifier.GetNotifier().Notify(SubscriberTag.PLAN_VIEW_TAG);
        }
    }
}
