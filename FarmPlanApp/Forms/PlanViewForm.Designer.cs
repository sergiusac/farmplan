﻿namespace FarmPlanApp.Forms
{
    partial class PlanViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.expensesDGV = new System.Windows.Forms.DataGridView();
            this.addExpenseTypeBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.expensesTabPage = new System.Windows.Forms.TabPage();
            this.removeExpenseTypeBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.incomesTabPage = new System.Windows.Forms.TabPage();
            this.incomesDGV = new System.Windows.Forms.DataGridView();
            this.resultsPage = new System.Windows.Forms.TabPage();
            this.summariesDGV = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.expensesDGV)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.expensesTabPage.SuspendLayout();
            this.incomesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.incomesDGV)).BeginInit();
            this.resultsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.summariesDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // expensesDGV
            // 
            this.expensesDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expensesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.expensesDGV.Location = new System.Drawing.Point(4, 32);
            this.expensesDGV.Margin = new System.Windows.Forms.Padding(2);
            this.expensesDGV.Name = "expensesDGV";
            this.expensesDGV.RowHeadersWidth = 62;
            this.expensesDGV.RowTemplate.Height = 28;
            this.expensesDGV.Size = new System.Drawing.Size(713, 320);
            this.expensesDGV.TabIndex = 0;
            this.expensesDGV.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.expensesDGV_CellBeginEdit);
            this.expensesDGV.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.expensesDGV_CellEndEdit);
            // 
            // addExpenseTypeBtn
            // 
            this.addExpenseTypeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.addExpenseTypeBtn.Location = new System.Drawing.Point(584, 357);
            this.addExpenseTypeBtn.Margin = new System.Windows.Forms.Padding(2);
            this.addExpenseTypeBtn.Name = "addExpenseTypeBtn";
            this.addExpenseTypeBtn.Size = new System.Drawing.Size(131, 23);
            this.addExpenseTypeBtn.TabIndex = 2;
            this.addExpenseTypeBtn.Text = "Добавить показатель";
            this.addExpenseTypeBtn.UseVisualStyleBackColor = true;
            this.addExpenseTypeBtn.Click += new System.EventHandler(this.addExpenseTypeBtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.expensesTabPage);
            this.tabControl1.Controls.Add(this.incomesTabPage);
            this.tabControl1.Controls.Add(this.resultsPage);
            this.tabControl1.Location = new System.Drawing.Point(8, 8);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(727, 405);
            this.tabControl1.TabIndex = 3;
            // 
            // expensesTabPage
            // 
            this.expensesTabPage.Controls.Add(this.removeExpenseTypeBtn);
            this.expensesTabPage.Controls.Add(this.label1);
            this.expensesTabPage.Controls.Add(this.addExpenseTypeBtn);
            this.expensesTabPage.Controls.Add(this.expensesDGV);
            this.expensesTabPage.Location = new System.Drawing.Point(4, 22);
            this.expensesTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.expensesTabPage.Name = "expensesTabPage";
            this.expensesTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.expensesTabPage.Size = new System.Drawing.Size(719, 379);
            this.expensesTabPage.TabIndex = 0;
            this.expensesTabPage.Text = "Расходы";
            this.expensesTabPage.UseVisualStyleBackColor = true;
            // 
            // removeExpenseTypeBtn
            // 
            this.removeExpenseTypeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.removeExpenseTypeBtn.Location = new System.Drawing.Point(449, 356);
            this.removeExpenseTypeBtn.Margin = new System.Windows.Forms.Padding(2);
            this.removeExpenseTypeBtn.Name = "removeExpenseTypeBtn";
            this.removeExpenseTypeBtn.Size = new System.Drawing.Size(131, 23);
            this.removeExpenseTypeBtn.TabIndex = 4;
            this.removeExpenseTypeBtn.Text = "Удалить показатель";
            this.removeExpenseTypeBtn.UseVisualStyleBackColor = true;
            this.removeExpenseTypeBtn.Click += new System.EventHandler(this.removeExpenseTypeBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(254)));
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(477, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Переменные затраты на производство сырого молока, тнг/кг";
            // 
            // incomesTabPage
            // 
            this.incomesTabPage.Controls.Add(this.incomesDGV);
            this.incomesTabPage.Location = new System.Drawing.Point(4, 22);
            this.incomesTabPage.Margin = new System.Windows.Forms.Padding(2);
            this.incomesTabPage.Name = "incomesTabPage";
            this.incomesTabPage.Padding = new System.Windows.Forms.Padding(2);
            this.incomesTabPage.Size = new System.Drawing.Size(719, 379);
            this.incomesTabPage.TabIndex = 1;
            this.incomesTabPage.Text = "Доходы";
            this.incomesTabPage.UseVisualStyleBackColor = true;
            // 
            // incomesDGV
            // 
            this.incomesDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.incomesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.incomesDGV.Location = new System.Drawing.Point(4, 4);
            this.incomesDGV.Margin = new System.Windows.Forms.Padding(2);
            this.incomesDGV.Name = "incomesDGV";
            this.incomesDGV.RowHeadersWidth = 62;
            this.incomesDGV.RowTemplate.Height = 28;
            this.incomesDGV.Size = new System.Drawing.Size(713, 371);
            this.incomesDGV.TabIndex = 0;
            this.incomesDGV.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.incomesDGV_CellBeginEdit);
            this.incomesDGV.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.incomesDGV_CellEndEdit);
            // 
            // resultsPage
            // 
            this.resultsPage.Controls.Add(this.summariesDGV);
            this.resultsPage.Location = new System.Drawing.Point(4, 22);
            this.resultsPage.Margin = new System.Windows.Forms.Padding(2);
            this.resultsPage.Name = "resultsPage";
            this.resultsPage.Size = new System.Drawing.Size(719, 379);
            this.resultsPage.TabIndex = 2;
            this.resultsPage.Text = "Расчеты";
            this.resultsPage.UseVisualStyleBackColor = true;
            // 
            // summariesDGV
            // 
            this.summariesDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.summariesDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.summariesDGV.Location = new System.Drawing.Point(4, 4);
            this.summariesDGV.Margin = new System.Windows.Forms.Padding(2);
            this.summariesDGV.Name = "summariesDGV";
            this.summariesDGV.RowHeadersWidth = 62;
            this.summariesDGV.RowTemplate.Height = 28;
            this.summariesDGV.Size = new System.Drawing.Size(713, 373);
            this.summariesDGV.TabIndex = 1;
            // 
            // PlanViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 421);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PlanViewForm";
            this.Text = "PlanViewForm";
            ((System.ComponentModel.ISupportInitialize)(this.expensesDGV)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.expensesTabPage.ResumeLayout(false);
            this.expensesTabPage.PerformLayout();
            this.incomesTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.incomesDGV)).EndInit();
            this.resultsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.summariesDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView expensesDGV;
        private System.Windows.Forms.Button addExpenseTypeBtn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage expensesTabPage;
        private System.Windows.Forms.TabPage incomesTabPage;
        private System.Windows.Forms.DataGridView incomesDGV;
        private System.Windows.Forms.TabPage resultsPage;
        private System.Windows.Forms.DataGridView summariesDGV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button removeExpenseTypeBtn;
    }
}