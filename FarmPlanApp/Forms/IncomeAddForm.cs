﻿using FarmPlanApp.Entities;
using FarmPlanApp.Notification;
using FarmPlanApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmPlanApp.Forms
{
    public partial class IncomeAddForm : Form
    {
        private Plan plan;

        public IncomeAddForm(Plan plan)
        {
            InitializeComponent();

            this.plan = plan;
            var years = PlanService.GetYears(plan);
            var months = PlanService.GetMonths(plan, years[0]);

            yearComboBox.DataSource = years;
            monthComboBox.DataSource = months;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            var volume = float.Parse(volumeTB.Text);
            var price = float.Parse(priceTB.Text);
            var planDate = PlanService.GetPlanDate(plan, (int)yearComboBox.SelectedItem, (int)monthComboBox.SelectedItem);
            SaleService.AddSale(planDate, volume, price);

            AppNotifier.GetNotifier().Notify(SubscriberTag.PLAN_VIEW_TAG);
            Close();
        }

        private void removeIncomeBtn_Click(object sender, EventArgs e)
        {
            var volume = -float.Parse(volumeTB.Text);
            var price = -float.Parse(priceTB.Text);
            var planDate = PlanService.GetPlanDate(plan, (int)yearComboBox.SelectedItem, (int)monthComboBox.SelectedItem);
            SaleService.AddSale(planDate, volume, price);

            AppNotifier.GetNotifier().Notify(SubscriberTag.PLAN_VIEW_TAG);
            Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void yearComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var year = (int)yearComboBox.SelectedItem;
            monthComboBox.DataSource = PlanService.GetMonths(plan, year);
        }
    }
}
