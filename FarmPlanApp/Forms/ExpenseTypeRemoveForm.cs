﻿using FarmPlanApp.Entities;
using FarmPlanApp.Notification;
using FarmPlanApp.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FarmPlanApp.Forms
{
    public partial class ExpenseTypeRemoveForm : Form
    {
        private Plan plan;

        public ExpenseTypeRemoveForm(Plan plan)
        {
            InitializeComponent();

            this.plan = plan;
            expenseTypeComboBox.DataSource = plan.ExpenseTypes;
            expenseTypeComboBox.ValueMember = "TypeName";
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            ExpenseType type = (ExpenseType)expenseTypeComboBox.SelectedItem;
            DialogResult result = MessageBox.Show(
                "Вы действительно хотите удалить показатель " + type.TypeName + "?",
                "Удалить показатель " + type.TypeName + "?",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2);
            if (result.Equals(DialogResult.Yes))
            {
                ExpenseTypeService.RemoveExpenseType(type);
                AppNotifier.GetNotifier().Notify(SubscriberTag.PLAN_VIEW_TAG);
                Close();
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
