﻿using FarmPlanApp.MVs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlanApp.Validators
{
    public class PlanValidator
    {
        public bool Validate(PlanMV plan)
        {
            var startDate = new DateTime(plan.StartYear, plan.StartMonth, 1);
            var endDate = new DateTime(plan.EndYear, plan.EndMonth, 1);
            
            var areDatesValid = startDate < endDate;
            
            var isNameValid = plan.Name.Length > 0;

            return isNameValid && areDatesValid;
        }
    }
}
